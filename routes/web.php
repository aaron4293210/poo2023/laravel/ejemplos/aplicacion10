<?php

use App\Http\Controllers\front\ProductoController;
use App\Http\Controllers\front\VendedorController;
use App\Http\Controllers\front\VentaController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::controller(ProductoController::class)->group(function () {
    Route::get('/producto', 'index')->name('producto.index');
    Route::get('/producto/{producto}', 'show')->name('producto.show');
});

Route::controller(VendedorController::class)->group(function () {
    Route::get('/vendedor', 'index')->name('vendedor.index');
    Route::get('/vendedor/{vendedor}', 'show')->name('vendedor.show');
});

Route::controller(VentaController::class)->group(function () {
    Route::get('/venta', 'index')->name('venta.index');
    Route::get('/venta/{venta}', 'show')->name('venta.show');
});
