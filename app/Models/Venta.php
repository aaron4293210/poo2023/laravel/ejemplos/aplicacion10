<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Venta extends Model {
    use CrudTrait;
    use HasFactory;

    protected $table = 'ventas';

    protected $fillable = [
        'producto_id',
        'vendedor_id',
        'cantidad',
    ];

    public function producto(): BelongsTo{
        return $this->belongsTo(Producto::class);
    }

    public function vendedor(): BelongsTo{
        return $this->belongsTo(Vendedor::class);
    }
}
