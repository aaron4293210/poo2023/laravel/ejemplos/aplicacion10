<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Vendedor extends Model {
    use CrudTrait;
    use HasFactory;

    protected $table = 'vendedores';

    protected $fillable = [
        'nombre',
        'apellidos',
        'correo'
    ];

    public function ventas(): HasMany {
        return $this->hasMany(Venta::class);
    }
}
