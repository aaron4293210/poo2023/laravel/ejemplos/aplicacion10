<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller {

    public function index() {
        return view('front.producto.index');
    }

    public function show(Producto $producto) {
        return view('front.producto.show');
    }
}
