<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\Vendedor;
use Illuminate\Http\Request;

class VendedorController extends Controller {

    public function index() {
        return view('front.vendedores.index');
    }

    public function show(Vendedor $vendedor) {
        return view('front.vendedores.show');
    }
}
