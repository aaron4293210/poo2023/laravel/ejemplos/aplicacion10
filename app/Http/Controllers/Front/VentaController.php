<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\Venta;
use Illuminate\Http\Request;

class VentaController extends Controller {

    public function index() {
        return view('front.venta.index');
    }

    public function show(Venta $venta) {
        return view('front.producto.show');
    }
}
