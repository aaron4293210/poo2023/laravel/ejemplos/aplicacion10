# OBJETIVO

Realizar una aplicacion completa con BackEnd y FrontEnd

Vamos a utilizar para realizar la parte del CRUD un paquete backpack

# SISTEMA DE INFORMACION

La tabla productos 
- nombre
- precio

La tabla ventas:
- producto
- vendedor
- cantidad

La tabla vendedores:
- nombre
- apellidos
- correo

# 1 paso

Realizar los modelos y migraciones para cada tabla

```php
php artisan make:model Producto -m
php artisan make:model Vendedor -m
php artisan make:model Venta -m
``` 

Ahora coloco en los modelos los campos de asignacion masiva

Ahora creo los campos en las migraciones. Creo las relaciones (claves ajenas)

# 2 Paso

Voy a colocar en los modelos unos metodos para implementar las relaciones






